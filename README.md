# tekton-first-pipeline
## Description
Cette pipeline va telecharger un repos sur gitlab, ajouter un fichier dans ce dernier et push la modification. 
## Tasks
### cleanup
Nettoie le workspace qui pourrait être remplis lors d'un précédent run de la pipeline.
### git-alpine-task

Cette tache va cloner un repos. Pour permettre cette action, nous allons monter en volume 
    le secret. Ensuite nous allons copier la cle ssh en root avec les bonnes permissions. 
    nous ajoutons StrictHostKeyChecking à no afin qu'il ne pose aucunes questions et ajoute automatiquement la nouvelle cle
    d hotes dans le fichier /known_hosts.
    Un workspace est utilisé ainsi que le volumemount qui nous sert a passer le secret contenant le rsa 
    et known_host de gitlab.

### git-push-task

Cette tache va nous permettre de push sur un repos git. Pour permettre cette action, nous allons monter en volume 
    le secret. Ensuite nous allons copier la cle ssh en root avec les bonnes permissions. 
    nous ajoutons StrictHostKeyChecking à no afin qu'il ne pose aucunes questions et ajoute automatiquement la nouvelle cle
    d hotes dans le fichier /known_hosts.
    Nous définissons un mail / username en plus.
    Un workspace est utilisé ainsi que le volumemount qui nous sert a passer le secret contenant le rsa et known_host de gitlab.
